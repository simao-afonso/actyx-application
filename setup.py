#!/usr/bin/env python
import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
        name='Actyx Challenges',
        version='1.0',
        author='Simão Afonso',
        author_email='simaopoafonso@gmail.com',
        description='The Actyx Challenges',
        long_description=read('README.md'),
        classifiers=[
            'Environment :: Web Environment',
            'Framework :: Django :: 1.8',
            'Programming Language :: Python :: 3.4',
            ],
        url='',
        packages=[
            'actyx1',
            ],
        install_requires=[
            'Django',
            'pytz',
            'urllib3',
            'whitenoise',
            ],
        )
