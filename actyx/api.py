#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ts=4:sw=4:et

import datetime
import time
from .util import JSON
import logging
logger = logging.getLogger(__name__)


class APIUnavailableException(Exception):
    """The API is not available at this moment"""


class APIActyx():
    TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'
    """A wrapper for the Actyx API"""
    def __init__(self,
                 root_url='http://machinepark.actyx.io/api',
                 version=1,
                 immediate=True,
                 ):
        self._root_url = root_url
        self.v = int(version)
        self.root = "%s/v%d" % (self._root_url, self.v)
        self.placeholders = {
                'API_ROOT': self.root,
                }
        self.endpoints = dict()
        if immediate:
            self.getEndpoints()

    def getEndpoints(self):
        e = JSON.request(self.root)
        self.endpoints = e or dict()
        return self.endpoints

    def _get(self, endpoint, extra_placeholders=None):
        """Get the URL for an endpoint"""
        all_placeholders = self.placeholders
        all_placeholders.update(extra_placeholders or {})
        if len(self.endpoints) == 0:
            # Try to fetch the endpoints again, the API
            # was probably busy
            self.getEndpoints()
        if endpoint in self.endpoints:
            raw_url = self.endpoints[endpoint]
            return self.__class__._process(raw_url, all_placeholders)

    def _process(url, placeholders):
        """Replace placeholders"""
        result = url
        for var in placeholders:
            replacement = placeholders[var]
            variable = '$'+var
            if replacement is not None:
                result = result.replace(variable, replacement)
        return result

    def _get_uuid(url):
        """
        Extract the UUID from a URL
        """
        return url[url.rfind('/')+1:]

    def _parse_timestamp(string):
        """
        Extract a float from the API string
        """
        try:
            dt = datetime.datetime.strptime(string, APIActyx.TIMESTAMP_FORMAT)
            return dt.timestamp()
        except Exception as e:
            # Parsing error, send a None
            logger.warn('Parse Timestamp', string)
            return None

    # "Public" API
    def format_timestamp(secs, fmt=None):
        """
        Extract a string representation of the float
        Up to microseconds precision
        `fmt` is a `time.strftime` format
        """
        try:
            dt = datetime.datetime.fromtimestamp(secs)
            return dt.strftime(fmt or APIActyx.TIMESTAMP_FORMAT)
        except Exception as e:
            # Parsing error, send a None
            logger.warn('Format Timestamp', secs, fmt)
            return None

    def streamMachinesUUID(self):
        """
        Get a list of machine UUID
        Returns a generator
        """
        cls = self.__class__
        response_url = self._get('machines_list')
        response = JSON.request(response_url)
        if not response or len(response) == 0:
            raise APIUnavailableException()
        for url in response:
            yield cls._get_uuid(url)

    def getMachinesUUID(self):
        """
        Get a list of machine UUID
        """
        return [uuid for uuid in self.streamMachinesUUID()]

    def getMachineData(self, uuid):
        """Get data for a certain machine"""
        cls = self.__class__
        response_url = self._get('machine_detail', {
            'MACHINE_UUID': uuid,
            })
        response = JSON.request(response_url)
        logger.info('Machine Data[%s]: %s' % (uuid, response))
        timestamp = self.__class__._parse_timestamp(response['timestamp'])
        logger.info('Machine Data[%s] - Timestamp: %s' % (uuid, timestamp))
        response['timestamp'] = timestamp
        return response
