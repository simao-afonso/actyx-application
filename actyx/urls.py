from django.conf.urls import include, url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.views.generic.base import RedirectView

urlpatterns = [
        # Root URL
        url(r'^$', RedirectView.as_view(
                pattern_name='challenge1:index',
                permanent=False,
                query_string=False,
                )),

        # Apps
        url(r'^backend/', include(admin.site.urls)),
        url(
                r'^challenge1/',
                include('actyx1.urls', namespace='challenge1'),
                )
]
