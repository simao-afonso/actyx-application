#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ts=4:sw=4:et

import urllib.request
import json
import time
from urllib3.util.retry import Retry
from urllib3.util.timeout import Timeout
from urllib3.util.request import make_headers
import urllib3
import logging
logger = logging.getLogger(__name__)


class JSON:
    def read(fname):
        if not file_exists(fname):
            return None
        with open(fname, 'r') as f:
            return json.load(f)
        return None

    def request(url):
        http = URL3Singleton.instance()
        try:
            r = http.request('GET', url)
            encoding = r.getheader('content-encoding') or 'utf-8'
            data = r.data.decode(encoding)
            return json.loads(data)
        except urllib3.exceptions.MaxRetryError as e:
            logger.warning('Retries exhausted')
            return {}
        except Exception as e:
            # Some other error
            logger.error('Could not request the url')
            return None


class URL3Singleton:
    _instance = None

    @classmethod
    def instance(cls):
        if cls._instance is None:
            retry_cfg = Retry(
                    total=5,  # Retry 5 times
                    backoff_factor=0.75,  # Delay consecutive retries
                    status_forcelist=[  # Retry on these status
                        429,  # Too Many Requests
                        ],
                    )
            cls._instance = urllib3.PoolManager(
                    num_pools=3,  # Not many hosts...
                    retries=retry_cfg,
                    headers=make_headers(keep_alive=True),
                    # Very aggressive connection caching
                    timeout=Timeout(total=60),  # seconds
                    )
        return cls._instance
