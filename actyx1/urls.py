from django.conf.urls import url

from . import views

urlpatterns = [
        # Root URL
        url(r'^$', views.index, name='index'),

        # See all machines
        url(r'^machines/', views.IndexView.as_view(), name='machines'),
        # Fetch data from the Actyx API
        url(r'^fetch/(\w*)', views.fetch, name='fetch_api'),
        # API
        url(r'^api/stream/(\w*)', views.api_stream, name='API_stream'),


        url(r'^test/', views.test, name='TST'),
        ]
