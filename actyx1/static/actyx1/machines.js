// vim: ts=2:sw=2:fdm=marker
function receiveChunk(oEvent) {
	api_response = oEvent.currentTarget.responseText;
	return;
}

function main() {
	var url = '';
	try {
		url = document.getElementById("link-api-endpoint").href;
		loadingInfo('Found the API URL', url);
	} catch (e) {
		newWarningApp("Can't find the API URL");
	}

	// Close the old XHR, if exists
	if (api_xhr) {
		api_xhr.abort();
	}
	// Get this chunk of data
	var xhr = new XMLHttpRequest();
	// xhr.responseType = 'arraybuffer';
	xhr.addEventListener('progress', receiveChunk);
	xhr.open('GET', url, true);
	xhr.send();
	api_xhr = xhr;
	loadingInfo('Requesting data');
	// for (var i=0;i<100;i++) {
	// 	newWarningApp('TEST', 'testing hovers');
	// 	newWarningMachine('UUID'+i, 120, 1000);
	// }
	api_response = '';
	current_chunk = 0;
}

function mainLoop() {
	var next_separator = api_response.indexOf(API_SEPARATOR, current_chunk);
	console.log('Next Separator', next_separator, 'Current', current_chunk);
	if (next_separator !== -1 && next_separator > current_chunk) {
		// There's new data
		var raw_chunk = api_response.slice(current_chunk, next_separator);
		try {
			var response = JSON.parse(raw_chunk);
			updatePage(response);
			current_chunk = next_separator;
		} catch (e) {
			console.log('The chunk [',current_chunk,',',next_separator,'] is not valid');
		}
	}
	else {
		// Try and find the next separator
		if (next_separator === current_chunk) {
			var next_chunk = next_separator;
			// Find the next non-separator character
			for (;next_chunk <= api_response.length && api_response.charAt(next_chunk) == API_SEPARATOR; next_chunk++){}
			console.log('Recalculate current:', next_chunk);
			current_chunk = next_chunk;
		}
	}
}

function updatePage(data) {
	if (data) {
		console.log('Data:', data);
		if ('error' in data) {
			console.log('ERROR in the API');
			// Warn the user
			newWarningApp('The API has an error!', 'Message: '+data['error']);
			addClass(document.getElementById('p_updated-date'), 'hidden');
			// Stop the XHR for now
			stopUpdating();
		}
		else {
			var last_updated = data['last_updated'];
			if (document.getElementById('updated-date').innerHTML == last_updated) {
				return; // Nothing to update
			}
			// Process the data as usual
			updateLastUpdated(last_updated);
			data_uuid = data['data'];
			for (var uuid in data_uuid) {
				updateUUID(uuid, data_uuid[uuid]);
			}
			warnings_uuid = data['warnings'];
			for (var uuid in warnings_uuid) {
				newWarningMachine(uuid, data_uuid[uuid][2], data_uuid[uuid][1], warnings_uuid[uuid]); //%, Current
			}
		}
	}
}

function updateLastUpdated(string) {
	console.log('Update Last updated:', string);
	var loading_messages = document.getElementById('p_updating');
	var parent_node = document.getElementById('p_updated-date');
	var node = document.getElementById('updated-date');
	if (node) {
		addClass(loading_messages, 'hidden');
		node.innerHTML = string;
		removeClass(parent_node, 'hidden');
	}
}

function updateUUID(uuid, values) {
	// console.log('Update UUID:', uuid);
	var main_node = document.getElementById('uuid_'+uuid);
	var meter = main_node.getElementsByTagName('meter')[0];
	meter.value = values[1]; // % Usage
	if (!values[0]) {
		// The machine is not on
		addClass(main_node, 'disabled');
	}
	else {
		removeClass(main_node, 'disabled');
	}
}

function confirmStopUpdating() {
	if (confirm('Do you really want to stop updating?')) {
		stopUpdating();
	}
}
function stopUpdating() {
	api_xhr.abort();
	removeClass(document.getElementById('updated-stop'), 'hidden');
	removeClass(document.getElementById('retry'), 'hidden');
}

function loadingInfo(string) {
	return loadingInfo(string, null);
}
function loadingInfo(string, hover) {
	var node = document.getElementById('p_updating');
	if (node) {
		var msg_node = document.createElement('span');
		if (hover) {
			msg_node.setAttribute('title', hover);
		}
		msg_node.appendChild(document.createTextNode(string));
		node.appendChild(msg_node);
		// Don't flood the bar
		if (node.children.length > 3) {
			// Remove the first (older) message
			node.children[0].remove();
		}
	}
}

function newWarningApp(text, hover) {
	var node = document.createElement('li');
	node_id = 'application';
	addClass(node, 'application');
	if (hover) {
			node.setAttribute('title', hover);
	}
	node.appendChild(document.createTextNode(text));
	newWarning(node_id, node);
}
function newWarningMachine(UUID, current, pct, historical) {
	var node = document.createElement('li');
	var node_id = 'warning_uuid_'+UUID;
	node.setAttribute('title', 'Current: '+current+'kW ('+pct+'%)');
	node.appendChild(document.createTextNode(UUID));
	var link = document.createElement('a');
	link.setAttribute('href', '#uuid_'+UUID);
	link.appendChild(document.createTextNode('GO'));
	node.appendChild(link);
	newWarning(node_id, node);
}
function newWarning(id, node) {
	var label = document.getElementById('p_warnings');
	var list = document.getElementById('list-warnings');
	if (list) {
		removeClass(label, 'hidden');
		removeClass(list, 'hidden');
		var old_node = document.getElementById(id);
		// Add the new warning at the top
		list.insertBefore(node, list.firstChild);
		if (old_node) {
			// There's an old warning with the same ID, remove it
			old_node.remove();
		}
		node.setAttribute('id', id);
	}
}

API_SEPARATOR = '\033'
var api_xhr = null;
var api_response = '';
var current_chunk = 0;

if (document.readyState == 'loading') {
	document.addEventListener('DOMContentLoaded', main);
}
else {
	setTimeout(main, 1000);
}

// Check if the API has returned data at every fifth of a second
var api_interval = setInterval(mainLoop, 1000/5);

// Setup the clicking areas
var stop = document.getElementById('updated-date');
stop.addEventListener('click', confirmStopUpdating);

// Helper functions {{{
function removeClass(node, cls) {
	var raw_classes = node.getAttribute('class');
	if (raw_classes) {
		var classes = raw_classes.split(' ');
		var result = classes.filter(function(element) {
			return element != cls;
		});
		var new_classes = result.join(' ');
		node.setAttribute('class', new_classes);
	}
}

function addClass(node, cls) {
	var raw_classes = node.getAttribute('class');
	var classes = [];
	if (raw_classes) {
		classes = raw_classes.split(' ');
	}
	classes.push(cls);
	var new_classes = classes.join(' ');
	node.setAttribute('class', new_classes);
}
//}}}
