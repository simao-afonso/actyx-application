url = document.getElementById("link-api-endpoint").href;

console.log('API URL',url);

// Get this chunk of data
var cur_length = 0;
var xhr = new XMLHttpRequest();
xhr.open('GET', url, true);
xhr.onprogress = function() {
	// Received a chunk
	if (xhr.readyState == 3) {
		raw_response = xhr.responseText;
		raw_length = raw_response.length;
		len = raw_length - cur_length;
		cur_response = raw_response.slice(cur_length, raw_length);
		console.log('Current Response:', cur_response);
		cur_length = raw_length; // Save the value for the next time

		response = JSON.parse(cur_response);
		console.log('Response:', response);
		updatePage(response);
	}
	else {
		console.log("Something's wrong\nXHR ReadyState: ", xhr.readyState);
	}
}
xhr.send();

// Loop through the data and update the page
function updatePage(data) {
	for (key in data) {
		var value = data[key];

		page_li = document.getElementById('li-'+key);
		if (page_li) {
			page_li.innerHTML = value;
		}
	}

	for (key in data) {
		var v = data[key];
		if (key > 10 && key < 12) {
			warn_li = document.createElement('li');
			warn_li.setAttribute('title', 'This is a tooltip');
			warn_li.innerHTML = 'This is a warning for value <b>'+v+'</b>, key <i>'+key+'</i>';
			warn_li.onclick = function(e) {
				this.remove();
			}
			document.getElementById('list-warnings').appendChild(warn_li);
		}
	}
}
