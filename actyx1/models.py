import datetime
from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone


class Machine(models.Model):
        """A machine tracked by the application"""
        uuid = models.CharField(
                        'Machine UUID',
                        primary_key=True,
                        max_length=37,  # According to the default format
                        )
        name = models.CharField('Machine name', max_length=500)
        machinetype = models.CharField('Machine type', max_length=100)
        location_x = models.FloatField(null=True)
        location_y = models.FloatField(null=True)

        def location(self):
                t = (self.location_x, self.location_y)
                if t == (None, None):
                        return None
                else:
                        return t
        location.short_description = 'The coordinates for the machine'

        def __str__(self):
                return 'mUUID: %s' % self.uuid


class MachinePark(models.Model):
        """A set of machines"""
        machines = models.ManyToManyField(Machine)

        def __str__(self):
                if self.id is None:
                        return 'MachinePark being built'
                return 'MachinePark %d' % self.id
