import time
import json

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import StreamingHttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone

from .models import *
from actyx.api import APIActyx, APIUnavailableException
import logging
logger = logging.getLogger(__name__)


def index(request):
        context = {
                        'title': 'Hello, World',
                        }
        return render(request, 'actyx1/index.django.html', context)


class IndexView(generic.ListView):
        model = Machine
        template_name = 'actyx1/machines.django.html'
        context_object_name = 'machines'
        ordering = 'name'

        def get_context_data(self, **kwargs):
                # Call the super function
                context = super(self.__class__, self).get_context_data(**kwargs)
                context['title'] = 'All Machines'
                return context


def fetch(request, selector):
        """Fetch all data from the Actyx API"""
        # return HttpResponseRedirect(reverse('challenge1:machines'))
        response = StreamingHttpResponse(fetch_generator(selector))
        return response


def fetch_generator(selector):
        yield '<h1>Actyx API Fetch</h1>'
        do = selector == 'ALL'
        if selector == 'NONE':
                raise Http404('Do nothing!')
        try:
                mp = MachinePark.objects.get(pk=1)
                yield '<h2>MachinePark Retrieved</h2>'
        except MachinePark.DoesNotExist as e:
                # Create the MachinePark
                mp = MachinePark.objects.create()
                yield '<h2>MachinePark Created</h2>'
        # Delete all existing machines
        for m in Machine.objects.all():
                m.delete()
        yield '<h2>Old machines deleted</h2>'
        api = APIActyx()
        yield '<h2>API object created</h2>'
        uuid_list = []
        try:
                uuid_list = api.streamMachinesUUID()
        except APIUnavailableException as e:
                yield '<b style="color:red;"><i>ERROR: %s</i></b>' % e
        yield '<ul>'
        for uuid in uuid_list:
                yield '<li>Machine UUID: <b>%s</b><br>' % uuid
                try:
                        metadata = api.getMachineData(uuid)
                        # Create a new Machine
                        m = Machine()
                        m.uuid = uuid
                        m.name = metadata['name']
                        m.machinetype = metadata['type']
                        location = [float(n) for n in metadata['location'].split(',')]
                        m.location_x = location[0]
                        m.location_y = location[1]
                        m.save()
                        mp.machines.add(m)
                        yield '<i>Done!</i>'
                except Exception as e:
                        yield '<b style="color:red;"><i>ERROR: %s</i></b>' % e
                finally:
                        yield '</li>'
        mp.save()
        yield '</ul>'
        yield '<h1>Done!</h1>'
        yield 'Get back <a href="/">Home</a>'


def api_stream(request, argument=None):
        """Fetch all data from the Actyx API"""
        response = StreamingHttpResponse(api_stream_generator(request, argument))
        response['Content-Type'] = 'application/json'
        return response


def api_stream_generator(reques, argument=None):
        """Stream the real-time current data as JSON objects"""
        api = APIActyx()
        try:
                uuid_list = api.getMachinesUUID()
                while True:
                        now = None
                        return_data = {'data': {}, 'warnings': {}}
                        for uuid in uuid_list:
                                metadata = api.getMachineData(uuid)
                                current = metadata['current']
                                current_alert = metadata['current_alert']
                                current_pct = round(100 * current / current_alert, 2)
                                working = metadata['state'] == 'working'
                                timestamp = metadata['timestamp']
                                if now is None:
                                        now = timestamp
                                else:  # Store the oldest updated timestamp
                                        if timestamp < now:
                                                now = timestamp
                                if working and current > current_alert:
                                        # It's over the maximum amount, warn the user
                                        return_data['warnings'][uuid] = -1 #TODO: rolling average of the last 5min
                                return_data['data'][uuid] = [working, current_pct, current]
                        return_data['last_updated'] = APIActyx.format_timestamp(now or time.time())
                        if len(return_data['data']) > 0:
                                yield json.dumps(return_data)
                                # This hack makes sure the servers flush their caches
                                # Use ESC (ASCII 033) so that it doesn't collide with non-binary data
                                yield '\033' * 1024
        except APIUnavailableException as e:
                yield json.dumps({'error': 'The API is not available at this moment'})
                # This hack makes sure the servers flush their caches
                # Use ESC (ASCII 033) so that it doesn't collide with non-binary data
                yield '\033' * 1024


def test(request):
        context = {
                        'title': 'API Test - Stream data',
                        'arr': range(1, 100),
                        }
        return render(request, 'actyx1/test_api.django.html', context)
