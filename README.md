# Actyx Challenges
This project contains the Actyx challenges.

## Challenge 1: Power Usage Alert
[See it here!](https://actyx-simaopoafonso.rhcloud.com/challenge1/machines/)

This app will monitor the Actyx Machine Park and warn you if any machine goes beyond the rated current.