DJANGO_MANAGE    = ./manage.py
DJANGO_RUNSERVER = runserver

default: check

check:
	${DJANGO_MANAGE} check

test:
	${DJANGO_MANAGE} test

server: title_server
	${DJANGO_MANAGE} ${DJANGO_RUNSERVER} localhost:8000
server_external: title_server
	${DJANGO_MANAGE} ${DJANGO_RUNSERVER} 0.0.0.0:8000

migrate:
	${DJANGO_MANAGE} makemigrations
	${DJANGO_MANAGE} migrate

admin:
	${DJANGO_MANAGE} createsuperuser --username 'admin' --email ''

deploy:
	git checkout deploy_openshift
	git rebase master
	git push -f openshift deploy_openshift:master
	# Back to the development
	git checkout master

clean: nuke migrate

nuke:
	${RM} db.sqlite3

title_server:
	echo -ne '\033]0;SERVER\007'

.PHONY: default check test server server_external migrate admin deploy clean nuke
